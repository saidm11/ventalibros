var terror =[
  {

    "imagen": "https://www.gonvill.com.mx/imagenes/9786074/978607457413.JPG",
    "titulo": "CUENTOS DE TERROR DE H.P. LOVECRAFT",
    "autor": "GUZMAN, RICARDO",
    "editorial":"LECTORUM",
    "costo": "119.20",
    "categoria":"Terror",
    "sinopsis":"En todo el mundo el nombre de Howard Philips Lovecraft es sinónimo de terror literarios y es considerado por muchos sectores sociales como un autor de culto. Y esto no es fortuito o una moda pasajera, sino el reflejo de su gran calidad literaria y su vasta imaginación. De hecho, los escritores de uno de los cómics que más éxito ha tenido en todo el mundo, fueron inspirados por la obra de Lovercraft para crear el Asilo (o Manicomio) Arkham, donde se recluyen personajes como El Guasón (o Jocker), El Pingüino, Dos Caras, Bane, El Acertijo... enemigos acérrimos de Batman.",
    "cantidad": 2


  },
  {

    "imagen": "https://www.gonvill.com.mx/imagenes/9786071/978607140838.JPG",
    "titulo": "TESTIMONIOS DE HORROR Y MISTERIO",
    "autor": "GOMEZ, YETZI",
    "editorial":"EDITORES MEXICANOS UNIDOS (EMU)",
    "costo": "17",
    "categoria":"Terror",
    "sinopsis":"",
    "cantidad": 2


  },
  {

    "imagen": "https://www.gonvill.com.mx/imagenes/9786071/978607141466.JPG",
    "titulo": "RELATOS DE MISTERIO Y TERROR -LB- (HIDRO)",
    "autor": "GOMEZ, ROBERTO",
    "editorial":"EDITORES MEXICANOS UNIDOS (EMU)",
    "costo": "15",
    "categoria":"Terror",
    "sinopsis":"Una banda de maleantes ha violado a centenares de mujeres; su sello distintivo es la cinta roja que dejan en la boca de sus víctimas. Gardenia, una reportera, ha investigado el asunto, sin éxito, y decide embriagar su fracaso en una cantina. De regreso a casa, ebria y triste, el transporte en el que viaja es asaltado por un par de sujetos que pretenden violarla como lo han hecho con tantas otras, pero no saben que en Gardenia se esconde un ser aterrador que les hará pagar por sus crímenes. Historias misteriosas, aterradoras, fantásticas y llenas de intriga te mostrarán la vida de personajes que se enfrentan con criaturas monstruosas, delincuentes despiadados y sucesos inexplicables. El llano de las sombras, La Xtabay o Las fauces del diablo, entre otros relatos, te harán pensar dos veces antes de viajar solo por las calles.",
    "cantidad": 2


  },
  {

    "imagen": "https://www.gonvill.com.mx/imagenes/9789706/978970627209.JPG",
    "titulo": "CUENTOS MEXICANOS DE TERROR",
    "autor": "VAGONES",
    "editorial":"Literatura de Terror y Misterio",
    "costo": "17",
    "categoria":"Terror",
    "sinopsis":"",
    "cantidad": 2


  },
  {

    "imagen": "https://www.gonvill.com.mx/imagenes/9789706/978970627267.JPG",
    "titulo": "CUENTOS MEXICANOS DE TERROR",
    "autor": "EPOCA",
    "editorial":"Literatura de Terror y Misterio",
    "costo": "17",
    "categoria":"Terror",
    "sinopsis":"",
    "cantidad": 2


  },
  {

    "imagen": "https://www.gonvill.com.mx/imagenes/9786074/978607457604.JPG",
    "titulo": "NOVELAS DE TERROR DE H.P. LOVECRAFT",
    "autor": "GUZMAN, RICARDO",
    "editorial":"LECTORUM",
    "costo": "127",
    "categoria":"Terror",
    "sinopsis":"Las obras de H.P. Lovecraft son consideradas de las mejores en la literatura fantástica. Combinando ciencia ficción y terror, crea atmósferas que envuelven al lector. Esta es una selección de las novelas más representativas, escogidas por el famoso escritor Ricardo Guzmán Wolffer, en donde se apreciará la madurez de este gran escritor.",
    "cantidad": 2


  },
  {

    "imagen": "https://www.gonvill.com.mx/imagenes/9781474/978147485339.JPG",
    "titulo": "COLOREE DESDE EL LADO OSCURO -TERROR-",
    "autor": "PARRAGON",
    "editorial":"PARRAGON",
    "costo": "171",
    "categoria":"Terror",
    "sinopsis":"En este libro le presentamos una selección de ilustraciones en blanco y negro, desde tenebrosos vampiros y espantosos espíritus hasta sirenas espeluznantes y bestias vengativas. Explore las profundidades de su imaginación y coloree estos seres malvados para que cobren vida. ¿Qué espera para llenar estas páginas de miedo y color?",
    "cantidad": 2


  },
  {

    "imagen": "https://www.gonvill.com.mx/imagenes/9786073/978607310552.JPG",
    "titulo": "IT (ESO)",
    "autor": "KING, STEPHEN",
    "editorial":"PLAZA Y JANES (DEBOLSILLO)",
    "costo": "447",
    "categoria":"Terror",
    "sinopsis":"¿Quién o qué mutila y mata a los niños de un pequeño pueblo norteamericano?\n" +
      "¿Por qué llega cíclicamente el horror a Derry en forma de un payaso siniestro que va sembrando la destrucción a su paso?\n" +
      "\n" +
      "Esto es lo que se proponen averiguar los protagonistas de esta novela. Tras veintisiete años de tranquilidad y lejanía, una antigua promesa infantil les hace volver al lugar en el que vivieron su infancia y juventud como una terrible pesadilla. Regresan a Derry para enfrentarse con su pasado y enterrar definitivamente la amenaza que los amargó durante su niñez.\n" +
      "Saben que pueden morir, pero son conscientes de que no conocerán la paz hasta que aquella cosa sea destruida para siempre.\n" +
      "Ites una de las novelas más ambiciosas de Stephen King, con la que ha logrado perfeccionar de un modo muy personal las claves del género de terror.\n" +
      "La crítica ha dicho...\n" +
      "«Insuperable.»\n" +
      "La Vanguardia",
    "cantidad": 2


  },
  {

    "imagen": "https://www.gonvill.com.mx/imagenes/9786073/978607311839.JPG",
    "titulo": "EL RESPLANDOR (EDICIÓN DE ANIVERSARIO)",
    "autor": "KING, STEPHEN",
    "editorial":"PLAZA Y JANES (DEBOLSILLO)",
    "costo": "372",
    "categoria":"Terror",
    "sinopsis":"\"REDRUM\". Ésa es la palabra que Danny había visto en el espejo. Y aunque no sabía leer, entendió que era un mensaje de horror.\n" +
      "\n" +
      "Una de las historias clásicas del maestro indiscutible de la narrativa de terror contemporánea, que inspiró la no menos clásica adaptación al cine de Stanley Kubrick,The Shining.\n" +
      "\"REDRUM\"\n" +
      "Ésa es la palabra que Danny había visto en el espejo. Y aunque no sabía leer, entendió que era un mensaje de horror. Danny tenía cinco años. Y a esa edad pocos niños saben que los espejos invierten las imágenes y, menos aún, saben diferenciar entre realidad y fantasía. Pero Danny tenía pruebas de que sus fantasías relacionadas con el resplandor del espejo acabarían cumpliéndose: \"REDRUM\"... \"MURDER\", asesinato.\n" +
      "Pero su padre necesitaba aquel trabajo en el hotel. Danny sabía que su madre pensaba en el divorcio y que su padre se obsesionaba con algo muy malo, tan malo como la muerte y el suicidio. Sí, su padre necesitaba aceptar la propuesta de cuidar aquel hotel de lujo con más de cien habitaciones, aislado por la nieve durante seis meses. Hasta el deshielo iban a estar solos.\n" +
      "\"Stephen King, el rey del terror.\"\n" +
      "El País\n" +
      "\"Un narrador magistral.\"\n" +
      "Los Angeles Times\n" +
      "\"El indiscutible maestro del suspenso y del terror.\"\n" +
      "The Washington Post",
    "cantidad": 2


  },
  {

    "imagen": "https://www.gonvill.com.mx/imagenes/9786073/978607311382.JPG",
    "titulo": "LA NIEBLA",
    "autor": "KING, STEPHEN",
    "editorial":"PLAZA Y JANES (DEBOLSILLO)",
    "costo": "281",
    "categoria":"Terror",
    "sinopsis":"El maestro se supera a sí mismo... en aterrar.\n" +
      "\n" +
      "He aquí una serie de cuentos -unos, horripilantes en su extravagancia; otros, tan terroríficos que disparan el corazón- que son el producto más acabado de una de las más poderosas imaginaciones de nuestro tiempo.\n" +
      "En el cuento que da título al libro, las puertas del infierno parecen abrirse para un pequeño pueblo de Maine llamado Long Lake. Un supermercado se convierte en el último bastión de su población pues una densa niebla comienza a devorarlo todo, y al parecer no viene sola. En otro, no menos perturbador, la tranquilidad le será negada a un personaje cuando descubra que su juguete, hace años desechado, ahora se presenta en su desván revelándose con siniestros rasgos.\n" +
      "En esta antología Stephen King nos demuestra que el terror sobrecogedor y cruel no sólo acecha en lo que viene de fuera, también en lo que no percibimos dentro de nosotros.\n" +
      "El maestro se supera a sí mismo... en aterrar.\n" +
      "He aquí una serie de cuentos -unos, horripilantes en su extravagancia; otros, tan terroríficos que disparan el corazón- que son el producto más acabado de una de las más poderosas imaginaciones de nuestro tiempo.\n" +
      "En el cuento que da título al libro, las puertas delinfierno parecen abrirse para un pequeño pueblo de Maine llamado Long Lake. Un supermercado se convierte en el último bastión de su población pues una densa niebla comienza a devorarlo todo, y al parecer no viene sola. En otro, no menos perturbador, la tranquilidad le será negada a un personaje cuando descubra que su juguete, hace años desechado, ahora se presenta en su desván revelándose con siniestros rasgos.\n" +
      "En esta antología Stephen King nos demuestra que el terror sobrecogedor y cruel nosólo acecha en lo que viene de fuera, también en lo que no percibimos dentro de nosotros.",
    "cantidad": 2


  },
]

var finanzas =[
  {

    "imagen": "https://www.gonvill.com.mx/imagenes/9786073/978607317887.JPG",
    "titulo": "LA SABIDURÍA DE LAS FINANZAS",
    "autor": "A. DESAI, MIHIR",
    "editorial":"TAURUS",
    "costo": "199",
    "categoria":"Finanzas",
    "sinopsis":"«Una nueva perspectiva fascinante de las finanzas modernas.»\n" +
      "\n" +
      "Oliver Hart, Premio Nobel de Economía 2016\n" +
      "Las finanzas han sacudido la economía una y otra vez. De manera similar, en la academia y en la práctica, éstas se han vuelto más especializadas, más difíciles de entender y más alejadas de las vidas de las personas, con un rasgo, incluso, queintimida a la gente.\n" +
      "Mihir A. Desai, profesor de Harvard Business School, propone un nuevo acercamiento. Con ironía e ingenio, recorre el mundo de la literatura, el cine, la historia y la filosofía para descubrir ellado humano de las finanzasy cómo éstas también ofrecen una visión sorprendente sobre aspectos cotidianos de nuestra humanidad. Entenderemos cómo el filósofo Charles Sanders Peirce y el poeta Wallace Stevens funcionan como guías para explicar las nociones deriesgo y seguros; cómo Lizzy Bennet, la protagonista deOrgullo y prejuicio,y Violet Effingham, dePhineas Finn,son excepcionalesadministradorasde riesgos; o cómo entender el modelo de valoración de activos financieros puede permitirnos descubrir la naturaleza del amor incondicional.\n" +
      "La sabiduría de las finanzasdelinea las ideas principales de las finanzassin una sola ecuación o gráfica, sólo con historias; ofreciendo una perspectiva nueva y refrescante sobre uno de los campos más complejos e incomprendidos del mundo.",
    "cantidad": 2


  },
  {

    "imagen": "https://www.gonvill.com.mx/imagenes/9786078/978607855294.JPG",
    "titulo": "PRACTICAS EDUCATIVAS INNOVADORAS EN CONTABILIDAD Y FINANZAS",
    "autor": "ANDRADE / SILVA",
    "editorial":"IMCP INSTITUTO MEXICANO DE CONTADORES P.",
    "costo": "270",
    "categoria":"Finanzas",
    "sinopsis":"",
    "cantidad": 2


  },



  {

    "imagen": "https://www.gonvill.com.mx/imagenes/9786078/978607855282.JPG",
    "titulo": "FINANZAS III -MERCADOS FINANCIEROS- (ED.2018)",
    "autor": "ICMP",
    "editorial":"IMCP INSTITUTO MEXICANO DE CONTADORES P.",
    "costo": "261",
    "categoria":"Finanzas",
    "sinopsis":"",
    "cantidad": 2


  },
  {

    "imagen": "https://www.gonvill.com.mx/imagenes/9786078/978607846399.JPG",
    "titulo": "FINANZAS CORPORATIVAS 2ED. -VALOR LLAVE PARA UNA ORGANIZACION-",
    "autor": "PACHECO, CARLOS",
    "editorial":"IMCP INSTITUTO MEXICANO DE CONTADORES P.",
    "costo": "252",
    "categoria":"Finanzas",
    "sinopsis":"",
    "cantidad": 2


  },
  {

    "imagen": "https://www.gonvill.com.mx/imagenes/9789588/978958869136.JPG",
    "titulo": "FINANZAS CORPORATIVAS 2ED. -VALOR LLAVE PARA UNA ORGANIZACION-",
    "autor": "CLC EDITORIAL",
    "editorial":"CLC EDITORIAL",
    "costo": "65",
    "categoria":"Finanzas",
    "sinopsis":"",
    "cantidad": 2


  },
  {

    "imagen": "https://www.gonvill.com.mx/imagenes/9788417/978841775739.JPG",
    "titulo": "50 CONCEPTOS SOBRE EL DINERO -FINANZAS, INVERSIONES Y BANCA-",
    "autor": "MARRON, DONALD",
    "editorial":"BLUME",
    "costo": "432",
    "categoria":"Finanzas",
    "sinopsis":"",
    "cantidad": 2


  },
  {

    "imagen": "https://www.gonvill.com.mx/imagenes/9786075/978607526904.JPG",
    "titulo": "FINANZAS BÁSICAS PARA NO FINANCIEROS (ED.2019)",
    "autor": "ORTIZ ANAYA, HÉCTOR",
    "editorial":"CENGAGE/THOMSON",
    "costo": "340",
    "categoria":"Finanzas",
    "sinopsis":"",
    "cantidad": 2


  },
  {

    "imagen": "https://www.gonvill.com.mx/imagenes/9786078/978607833176.JPG",
    "titulo": "FINANZAS BURSATILES",
    "autor": "AYALA / BECERRIL",
    "editorial":"IMCP INSTITUTO MEXICANO DE CONTADORES P.",
    "costo": "283",
    "categoria":"Finanzas",
    "sinopsis":"",
    "cantidad": 2


  },

  {

    "imagen": "https://www.gonvill.com.mx/imagenes/9786075/978607524330.JPG",
    "titulo": "20 INNOVACIONES Y MEJORES PRACTICAS EN FINANZAS PUBLICAS",
    "autor": "BAEZ MAGAÑA, EMILIO",
    "editorial":"MIGUEL ANGEL PORRUA",
    "costo": "371",
    "categoria":"Finanzas",
    "sinopsis":"",
    "cantidad": 2


  },

  {

    "imagen": "https://www.gonvill.com.mx/imagenes/9786077/978607747714.JPG",
    "titulo": "FINANZAS PARA EMPRENDEDORES",
    "autor": "MANZANERA ESCRIBANO, ANTONIO",
    "editorial":"BOOKET",
    "costo": "299",
    "categoria":"Finanzas",
    "sinopsis":"Buscar capital para una empresa de nueva creación requiere no sólo de un sólido plan de negocio sino también de unos conocimientos de finanzas que permitan al emprendedor realizar una planificación financiera sensata, cerrar acuerdos de inversión en unos términos ventajosos para sus intereses y ofrecer seguridad y confianza al inversor. En Finanzas para emprendedores Antonio Manzanera describe todo aquello que un emprendedor debe conocer para conseguir financiación para su empresa y mantenerla a flote durante sus primeros años de vida. Si estás en el proceso de crear una empresa o bien la has creado recientemente y necesitas financiación para la misma, éste es el libro que necesitas para llevar tu proyecto a buen puerto.",
    "cantidad": 2


  },

]
var autoayuda =[
  {

    "imagen": "https://www.gonvill.com.mx/imagenes/9786073/978607317975.JPG",
    "titulo": "MANUAL DE AUTOAYUDA DE VLADIMIR PUTIN",
    "autor": "SEARS, ROB",
    "editorial":"DEBATE",
    "costo": "170",
    "categoria":"Autoayuda",
    "sinopsis":"¿Qué puede hacer Vadimir Putin por tu vida personal?\n" +
      "\n" +
      "Una guía para convertirte en el dictador que siempre has querido ser, inspirada en nuestro autócrata preferido.\n" +
      "¿Cansado de tus problemas de pareja?¿Tus hijos adolescentes te ignoran? ¿Tu jefe es un incompetente? ¿El chat del cole te parece un auténtico disparate?\n" +
      "Todas estas preguntas tienen una sola respuesta: sé más Vlad.\n" +
      "Recupera el control de tu vida mundana gracias a esta inigualable guía inspirada en décadas de práctica efectiva y de manipulación estratégica de nuestro autócrata preferido.\n" +
      "Lleno de anécdotas, consejos, ideas e ilustraciones para emular a pequeña escala a Vladímir Putin, Rob Sears te trae la guía perfecta para convertirte en el tirano que siempre has querido ser.",
    "cantidad": 2


  },
  {

    "imagen": "https://www.gonvill.com.mx/imagenes/9786071/978607112717.JPG",
    "titulo": "PEQUEÑO CERDO CAPITALISTA. INVERSIONES",
    "autor": "MACÍAS, SOFÍA",
    "editorial":"AGUILAR",
    "costo": "281",
    "categoria":"Autoayuda",
    "sinopsis":"Lo que este libro busca es que conozcas bien las alternativas para escoger la mejor manera de hacer que tu dinero produzca de acuerdo con cuánto riesgo quieras asumir, cuánto te quieras clavar en el tema y cuánto le puedas dedicar, para que vayas construyendo riqueza real y no accidental.\n" +
      "\n" +
      "Pequeño Cerdo Capitalistaha alcanzado los 100.000 ejemplares en sólo un año y medio.\n" +
      "Después del impactante éxito editorial dePequeño Cerdo Capitalista, Sofía Macías presenta este libro que te hará ver el dinero desde otra perspectiva y lo que es mejor, te ayudará a sacarle un gran provecho a tus ahorros.\n" +
      "Hay quienes aprendieron delPequeño Cerdo Capitalista, ¡y lograron ahorrar! Pero entonces llega la pregunta: ¿Y ahora qué hago con el dinero que tengo ahorrado? ¿Lo dejo abajo del colchón? ¿Me lo gasto antes de que valga menos? ¿Los cambio por un premio junto con taparroscas?... O ¿en serio puedo empezar a invertir mis \"pesitos\"?\n" +
      "Pequeño Cerdo Capitalista: Inversionesempieza por aclarar que \"se hacen inversiones, no milagros\", así que para obtener rendimientos sostenidos y reales, primero hay que grabarnos en la cabezota que requerimos tres cosas: tiempo, conocimientos y paciencia.\n" +
      "El objetivo de este libro es que entiendas qué son en realidad las inversiones, conozcas tu perfil de inversionista, construyas tu estrategia de inversión, entiendas qué hay detrás de cada opción para que elijas las que embonen más con tus metas y que aprendas lo más posible cuándo hacer cambios y cuándo no alocarte.\n" +
      "Recuerda que esto de las inversiones es un poco como aprender un nuevo idioma: puede que al principio solo balbuceemos o podamos hilar frases cortas y simples, pero conforme entendemos más de las palabras, cómo interactúan unas con otras y cuáles sirven para qué, el lenguaje cobra vida, se vuelve más útil y podemos usarlo para crear lo que queramos.",
    "cantidad": 2


  },


  {

    "imagen": "https://www.gonvill.com.mx/imagenes/9789706/978970666719.JPG",
    "titulo": "TU PUEDES SANAR A TRAVES DE LA MENTE",
    "autor": "SOSA, ALIDA",
    "editorial":"TOMO",
    "costo": "79",
    "categoria":"Autoayuda",
    "sinopsis":"",
    "cantidad": 2


  },
  {

    "imagen": "https://www.gonvill.com.mx/imagenes/7502275/750227567104.JPG",
    "titulo": "PSEUDOSICOLOGIA -EXPLORANDO LOS MITOS DE LA AUTOAYUDA-",
    "autor": "BRIERS, SRHEPHEN",
    "editorial":"QUARZO",
    "costo": "119",
    "categoria":"Autoayuda",
    "sinopsis":"",
    "cantidad": 2


  },



  {

    "imagen": "https://www.gonvill.com.mx/imagenes/9786077/978607707992.JPG",
    "titulo": "AUTOAYUDA PARA PADRES",
    "autor": "CALERO, MAVILO",
    "editorial":"ALFAOMEGA",
    "costo": "166",
    "categoria":"Autoayuda",
    "sinopsis":"Es un hecho que no existe una escuela que enseñe a las personas a ser padres, esto es, por el contrario, una tarea ardua que requiere de integridad humana, una amplia visión de objetivos, práctica y deseos de heredar valores y destrezas que les permitan a los hijos lograr la autosuficiencia y el completo desarrollo de sus aptitudes. Bajo el compromiso de ayudar a los padres a mejorar sus habilidades en el terreno familiar y propiciar un acercamiento entre sus miembros, el autor de este libro presenta una serie de consejos que desde la pedagogía y la didáctica facilitarán la creación de vínculos afectivos sólidos. El lector conseguirá ampliar su panorama sobre los problemas vitales que enfrentan los hijos en las diferentes etapas de su desarrollo ayudará a formar personas humanistas, con valores que alcancen su independencia viviendo en plenitud y felicidad a través del método aprender a aprender. Este material incluye fichas con información relevante, consejos útiles para padres, frases motivadoras, un decálogo de desarrollo para la familia y el Decálogo de los Derechos de los niños. El autor muestra cómo a través de las técnicas de autoayuda, como la del espejo, podemos prepararnos y convertirnos en guías y ejemplos en la crianza de los hijos, ayudarlos en las tareas escolares, prepararlos para la presentación de exámenes y, en resumen, enfrentarse a la vida misma.",
    "cantidad": 2


  },

  {

    "imagen": "https://www.gonvill.com.mx/imagenes/7502268/750226818928.JPG",
    "titulo": "PAQUETE AUTOAYUDA 2 (C/3 LIBROS) (DIVORCIADA/A SOLAS)",
    "autor": "DIANA",
    "editorial":"DIANA",
    "costo": "171",
    "categoria":"Autoayuda",
    "sinopsis":"",
    "cantidad": 2


  },
  {

    "imagen": "https://www.gonvill.com.mx/imagenes/9786073/978607319660.JPG",
    "titulo": "¡HAZLO!",
    "autor": "GODIN, SETH",
    "editorial":"EDICIONES B",
    "costo": "109",
    "categoria":"Autoayuda",
    "sinopsis":"¿Un libro en el que encontrarás el verdadero sentir de tu vida, ayudándote a tomar las decisiones correctas para iniciar de nuevo?\n" +
      "\n" +
      "Llevamos a nuestros hijos al colegio y nos obsesionamos con sus notas, su comportamiento y su capacidad de integración. Colgamos una oferta de trabajo y buscamos experiencia, universidades de prestigio y una carrera sin fracasos. Y entonces, ¿por qué nos sorprendemos cuando todo se desmorona? Nuestra economía no es estática, pero actuamos como si lo fuera. Tu posición en el mundo se define en función de lo que emprendes, de cómo lo haces y de lo que aprendes de los acontecimientos que causas.\n" +
      "¡Hazlo!Constituye un manifiesto sobre la producción de algo que escasea y es, por lo tanto, valioso.Ha llegado el momento de que dejes de esperar aque alguien te proporcione un mapa del camino y empieces a dibujarlo tú mismo.\n" +
      "Este libro quizá te haga sentir incómodo. Es una llamada de atención sobre las iniciativas que estás tomando, en el trabajo y en cuanto te concierne. Pero también puede ser el puntapié que necesitas para introducir un cambio en tu vida.",
    "cantidad": 2


  },

  {

    "imagen": "https://www.gonvill.com.mx/imagenes/9786075/978607562101.JPG",
    "titulo": "TRAMPAS DEL MIEDO, LAS",
    "autor": "HABIF, DANIEL",
    "editorial":"HARPER COLLINS PUBLISHERS",
    "costo": "349",
    "categoria":"Autoayuda",
    "sinopsis":"Las trampas del miedo es un viaje a las profundidades de nuestro ser, es una expedición por las dimensiones biológicas, psicológicas y espirituales de los temores que debemos desmantelar. Este libro posee un arsenal de información que te permitirá comprender por qué se producen esas reacciones que no puedes evitar. En este libro encontrarás conocimientos que te llevarán a establecer una nueva relación con tus detonantes interiores, podrás abrir los planos de la arquitectura de tu cuerpo y atravesarás el laberinto de tus emociones. No te quedarás en las expresiones primarias del miedo: explorarás el poder que este tiene en otros aspectos de tu vida como la soledad, la permanencia en el dolor, la ausencia de control y el rechazo al amor. En esta segunda publicación, Daniel Habif conecta su discurso energizante con una recopilación de investigaciones que han revelado fascinantes misterios del pensamiento. Estas páginas combinan la pasión de su verbo encendido con datos de interés y sencillos ejercicios que te servirán de guía para desarmar los trucos que se esconden en la fabulosa perfección de nuestra mente. Las trampas del miedo es más que enseñanzas, es una obra inspiradora; es una invitación a convertir los temores que hoy te detienen en el más potente de tus motores.",
    "cantidad": 2


  },

  {

    "imagen": "https://www.gonvill.com.mx/imagenes/9788434/978843447045.JPG",
    "titulo": "EROTICA DE LA AUTOAYUDA",
    "autor": "VIÑAS PIQUER, DAVID",
    "editorial":"ARIEL",
    "costo": "342",
    "categoria":"Autoayuda",
    "sinopsis":"Casi seguro, querido lector, que alguna vez se habrá sentido tentado a leer uno de esos libros que ofrecen respuestas a sus problemas. No se sienta culpable porque es muy difícil no toparse, hasta en los lugares más inopinados, con una obra de uno de los géneros literarios que más vende desde hace décadas, la autoayuda Un género heterogéneo por definición, tanto en el pedigrí de sus autores psicólogos, terapeutas, pedagogos, pr edicadores, ejecutivos, científicos y santones como por la variedad de temas que abordan; sin embargo, pueden rastrearse rasgos comunes: estrategias discursivas, mecanismos retóricos e incluso esquemas ideológicos similares; y al desvelamiento de esos rasgos se aplica, con humor e inteligencia, este libro del profesor David Viñas.",
    "cantidad": 2


  },
  {

    "imagen": "https://www.gonvill.com.mx/imagenes/9786071/978607110784.JPG",
    "titulo": "PEQUEÑO CERDO CAPITALISTA",
    "autor": "MACÍAS, SOFÍA",
    "editorial":"AGUILAR",
    "costo": "256",
    "categoria":"Autoayuda",
    "sinopsis":"Sofía Macías enseña de manera sencilla cómo obtener el mejor beneficio del dinero. Incluso para quienes creen que son un desastre en finanzas.\n" +
      "\n" +
      "Libro bestseller.\n" +
      "¿Quieres sacarle el mejor provecho a tu dinero? Entonces este libro es para ti.\n" +
      "Da igual que seas un trotamundos, especialista en ciencias ocultas, ejecutivo encorbatado, bohemio o arquitecto de tu propio destino: puedes utilizar tu dinero de una forma inteligente.\n" +
      "ConPequeño cerdo capitalistatendrás las herramientas más efectivas para ahorrar, invertir y usar de la mejor manera tus ingresos. Sabrás cómo hacer planes para financiar tus metas, cuánto recibirás de jubilación de Seguridad Social, cuáles son tus prestamos, derechos ysi es legal el cobrador del frac.\n" +
      "Además, conocerás cómo hacer un plan de pagos si tienes problemas de deudas, evitar caer en fraudes y chiringuitos financieros, sortear las épocas de vacas flacas si eres autónomo, qué es la bolsa y otras opciones para rentabilizar tu dinero.",
    "cantidad": 2


  },


]
var carrito=[


]
var comprobador=new number;
var contador=0;
/*
function cambiarf(){
  // document.getElementById("terror").innerHTML="Te cambie"
  console.log(comprobador)
  for(let i in finanzas)
  {

    let libro = document.createElement("div");
    libro.style.cssText='float:left; text-align:center';
    //libro.className="tam";
    libro.style="height:40%; width: 30%; float:left;"
    let imagen = document.createElement("img");
    imagen.srcset=finanzas[i].imagen;
    imagen.style="height:auto; max-width:60%";

    let titulo = document.createElement("div");
    titulo.textContent=finanzas[i].titulo;
    titulo.style.cssText='font-weight:bold';

    let precio= document.createElement("p");
    precio.textContent=finanzas[i].costo;

    const button=document.createElement('button');
    button.type='button';
    button.className="btn btn-primary"
    button.innerText="Agregar al carrito";
    button.setAttribute("id","idPrueba");

    libro.appendChild(imagen);
    libro.appendChild(titulo);
    libro.appendChild(precio);
    libro.appendChild(button);
    let ele= document.getElementById("info");
    ele.appendChild(libro);

    button.onclick = function () {
      comprobador=14;
      window.location.href = "detalle.html";

    }

  }
}*/
